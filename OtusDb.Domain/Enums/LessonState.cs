﻿using System.ComponentModel;

namespace OtusDb.Domain.Enums
{
    public enum LessonState
    {
        None = 0, 
        Сompleted = 1,
        NotСompleted = 2
    }
}