﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusDb.Domain.Enums
{
    public enum UserStatus
    {
        None = 0, 
        IsStudent = 1,
        IsTeacher = 2
    }
}
