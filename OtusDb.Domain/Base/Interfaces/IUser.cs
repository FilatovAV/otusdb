﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using OtusDb.Domain.Enums;

namespace OtusDb.Domain.Base.Interfaces
{
    public interface IUser
    {
        [StringLength(300)]
        string Email { get; set; }
        [Required]
        [StringLength(100)]
        string Login { get; set; }
        [Required]
        [StringLength(300)]
        string FirstName { get; set; }
        [StringLength(300)]
        string LastName { get; set; }
        [StringLength(300)]
        string Patronymic { get; set; }
        DateTime BirthDate { get; set; }
        [Required]
        DateTime RegistrationDateUTC { get; set; }
        [Required]
        UserStatus Status { get; set; }
    }
}
