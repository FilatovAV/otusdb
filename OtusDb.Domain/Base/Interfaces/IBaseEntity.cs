﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusDb.Domain.Base.Interfaces
{
    /// <summary> Сущность (база) </summary>
    public interface IBaseEntity
    {
        /// <summary> Идентификатор </summary>
        int Id { get; set; }
    }
}
