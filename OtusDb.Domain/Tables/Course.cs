﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OtusDb.Domain.Base;

namespace OtusDb.Domain.Tables
{
    [Table("Courses")]
    public class Course: BaseEntity
    {
        [Required]
        [StringLength(500)]
        public string Name { get; set; }
        public virtual ICollection<Lesson> Lessons { get; set; }
        public DateTime DateStartUTC { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }
    }
}