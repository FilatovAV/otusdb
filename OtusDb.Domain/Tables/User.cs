﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using OtusDb.Domain.Base;
using OtusDb.Domain.Base.Interfaces;
using OtusDb.Domain.Enums;

namespace OtusDb.Domain.Tables
{
    [Table("Users")]
    public class User: BaseEntity, IUser
    {
        [StringLength(200)]
        public string Email { get; set; }
        [StringLength(100)]
        public string Login { get; set; }
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(100)]
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime RegistrationDateUTC { get; set; }
        public UserStatus Status { get; set; }
        public virtual IEnumerable<CourseCollection> Courses { get; set; }
    }
}
