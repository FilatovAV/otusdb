﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OtusDb.Domain.Base;
using OtusDb.Domain.Enums;

namespace OtusDb.Domain.Tables
{
    [Table("Lessons")]
    public class Lesson: BaseEntity
    {
        [Required]
        [StringLength(500)]
        public string Name { get; set; }
        public string Theme { get; set; }
        public int TeacherId { get; set; }
        public User Teacher { get; set; }
        public LessonState State { get; set; }
        public DateTime DateStartUTC { get; set; }
        public float Score { get; set; }
        public virtual ICollection<Resource> Resources { get; set; }
        public string Homework { get; set; }
        public int CourseId { get; set; }
    }
}
