﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using OtusDb.Domain.Base;

namespace OtusDb.Domain.Tables
{
    [Table("Resources")]
    public class Resource: BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public string PathToFile { get; set; }
        public string Url { get; set; }
        public string Commentary { get; set; }
        public int LessonId { get; set; }
        public Lesson Lesson { get; set; }
    }
}
