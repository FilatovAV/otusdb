﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using OtusDb.Application;
using OtusDb.DAL.Repositories;
using OtusDb.Domain.Tables;

namespace OtusDb.WpfApp
{
    public class MainViewModel
    {
        private readonly OtusDbRepository _db;
        public ObservableCollection<User> Users { get; set; }
        public ObservableCollection<Course> Courses { get; set; }
        public ObservableCollection<Lesson> Lessons { get; set; }
        public ObservableCollection<User> Teachers { get; set; }
        public ObservableCollection<Resource> Resources { get; set; }
        public ObservableCollection<Course> SelectedUserCourses { get; set; }
        public ObservableCollection<Lesson> SelectedCourseLessons { get; set; }
        public ObservableCollection<Resource> SelectedLessonResources { get; set; }

        public MainViewModel(OtusDbRepository db)
        {
            _db = db;
            Users = new ObservableCollection<User>(_db.GetAllUser());
            Courses = new ObservableCollection<Course>(_db.GetAllCourses());
            Lessons = new ObservableCollection<Lesson>(_db.GetAllLessons());
            Teachers = new ObservableCollection<User>(_db.GetAllTeachers());
            Resources = new ObservableCollection<Resource>(_db.GetAllResources());
            SelectedUserCourses = new ObservableCollection<Course>();
            SelectedCourseLessons = new ObservableCollection<Lesson>();
            SelectedLessonResources = new ObservableCollection<Resource>();
        }

        public void SaveAll()
        {
            _db.SaveAll();
        }

        public void SelectedUserCoursesUpdateRange(IEnumerable<Course> courses)
        {
            SelectedUserCourses?.Clear();
            if (courses is null) { return; }
            foreach (var item in courses)
            {
                SelectedUserCourses.Add(item);
            }
        }
        public void SelectedCourseLessonsUpdateRange(IEnumerable<Lesson> lessons)
        {
            SelectedCourseLessons?.Clear();
            if (lessons is null) { return; }
            foreach (var item in lessons)
            {
                SelectedCourseLessons.Add(item);
            }
        }
        public void SelectedLessonResourcesUpdateRange(IEnumerable<Resource> resources)
        {
            SelectedLessonResources?.Clear();
            if (resources is null) { return; }
            foreach (var item in resources)
            {
                SelectedLessonResources.Add(item);
            }
        }
        //
    }
}
