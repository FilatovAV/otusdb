﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OtusDb.Application;
using OtusDb.DAL.Context;
using OtusDb.DAL.Repositories;

namespace OtusDb.WpfApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            IServiceCollection container = new ServiceCollection();

            var ApplicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var cs = $@"Data Source = (localdb)\MSSQLLocalDB; AttachDbFilename={ApplicationPath}\OtusDb.mdf;Integrated Security = True";

            container.AddDbContext<OtusDbContext>(options => options.UseSqlServer(cs));
            container.AddScoped<OtusDbRepository>();
            container.AddScoped<MainViewModel>();

            IServiceProvider serviceProvider = container.BuildServiceProvider();

            var mainWindow = new MainWindow()
            {
                DataContext = serviceProvider.GetService<MainViewModel>()
            };

            mainWindow.Show();
        }
    }
}
