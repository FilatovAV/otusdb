﻿using OtusDb.Domain.Tables;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OtusDb.WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //public MainWindow(MainViewModel mainViewModel)
        //{
        //    InitializeComponent();
        //    DataContext = mainViewModel;
        //}
        private MainViewModel mainViewModel;
        public MainWindow()
        {
            InitializeComponent();
            Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!(this.DataContext is MainViewModel model)) { return; }
            model.SaveAll();
        }

        private void LoginList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var u = LoginList.SelectedItem as User;
            mainViewModel.SelectedUserCoursesUpdateRange(u?.Courses.Select(s => s.Course));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            mainViewModel = DataContext as MainViewModel;
        }

        private void CourseList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var c = CourseList.SelectedItem as Course;
            mainViewModel.SelectedCourseLessonsUpdateRange(c?.Lessons);
        }

        private void LessonList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var l = LessonList.SelectedItem as Lesson;
            TeachersCb.SelectedItem = l?.Teacher;
            mainViewModel.SelectedLessonResourcesUpdateRange(l?.Resources);
        }
    }
}
