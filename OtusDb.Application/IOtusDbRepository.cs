﻿using OtusDb.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace OtusDb.Application
{
    public interface IOtusDbRepository
    {
        void AddNewUser(User user);
        void AddNewLesson(Lesson lesson);
        IEnumerable<Lesson> GetAllLessons();
        IEnumerable<Course> GetAllCourses();
        Course GetCourseById(int id);
        IEnumerable<User> GetAllTeachers();
        IEnumerable<Resource> GetAllResources();
        User GetUserByLogin(string login);
        void SaveAll();
    }
}
