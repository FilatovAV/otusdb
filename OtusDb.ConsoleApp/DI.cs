﻿using System;
using System.Collections.Generic;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Microsoft.EntityFrameworkCore;
using OtusDb.Application;
using OtusDb.DAL.Context;
using OtusDb.DAL.Repositories;

namespace OtusDb.ConsoleApp
{
    public static class DI
    {
        static string ApplicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static string cs = $@"Data Source = (localdb)\MSSQLLocalDB; AttachDbFilename={ApplicationPath}\OtusDb.mdf;Integrated Security = True";

        public static readonly IWindsorContainer Container;
        static DI()
        {
            Container = new WindsorContainer();
        }
        public static IWindsorContainer SetInjection()
        {
            var options = new DbContextOptionsBuilder<OtusDbContext>();
            options.UseSqlServer(cs);

            return Container
                //.AddDbContext<OtusDbContext>(options.Options, "EfContext")
                //.AddDbRepository<IOtusDbRepository, OtusDbRepository>("DbRepository", "EfContext")
                .AddOtusDbAdoRepository()
                .AddOtusService();
        }
        static IWindsorContainer AddDbContext<T>(this IWindsorContainer container, DbContextOptions<OtusDbContext> opt, string name)
        {
            return container.Register(Component.For(typeof(T))
                .Named(name)
                .ImplementedBy(typeof(T))
                .DependsOn(new { options = opt }));
        }
        static IWindsorContainer AddDbRepository<T, T1>(this IWindsorContainer container, string dependencyName, string componentName)
        {
            return container.Register(Component.For(typeof(T))
                .ImplementedBy(typeof(T1))
                .DependsOn(Dependency.OnComponent(dependencyName, componentName)));
        }
        static IWindsorContainer AddOtusDbAdoRepository(this IWindsorContainer container)
        {
            return container.Register(Component.For<IOtusDbRepository>().ImplementedBy<OtusDbAdoRepository>().DependsOn(new { connectionString = cs }));
        }
        static IWindsorContainer AddOtusService(this IWindsorContainer container)
        {
            return container.Register(Component.For<OtusService>());
        }
    }
}
