﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusDb.ConsoleApp
{
    public class ConsoleService
    {
        public void Message(string message)
        {
            Console.WriteLine(message);
        }
        public void MessageOnLine(string message)
        {
            Console.Write(message);
        }

        public string Read()
        {
            return Console.ReadLine();
        }
    }
}
