﻿using System;

namespace OtusDb.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DI.SetInjection();
            var service = DI.Container.Resolve<OtusService>();
            service.StartDialog();
        }
    }
}
