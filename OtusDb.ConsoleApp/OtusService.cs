﻿using System;
using System.Collections.Generic;
using System.Text;
using OtusDb.Application;
using OtusDb.DAL.Repositories;
using OtusDb.Domain.Base.Interfaces;
using OtusDb.Domain.Enums;
using OtusDb.Domain.Tables;

namespace OtusDb.ConsoleApp
{
    public class OtusService
    {
        private readonly ConsoleService _cs;
        private readonly OtusDbAdoRepository _dbAdo;

        public OtusService(IOtusDbRepository otusDbAdo)
        {
            _cs = new ConsoleService();
            _dbAdo = otusDbAdo as OtusDbAdoRepository;
        }
        internal void StartDialog()
        {
            _cs.Message("Добро пожаловать в Otus!\n\n");

            while (true)
            {
                EnterLogin();
                var result = _cs.Read();
                if (string.IsNullOrEmpty(result))
                {
                    return;
                }
                if (result.ToUpper() == "N")
                {
                    AddNewUser();
                    _cs.Message("Для входа в систему введите учетные данные.\n\n");
                    EnterLogin();
                    result = _cs.Read();
                }
                var user = _dbAdo.GetUserByLogin(result);
                if (user is null)
                {
                    UserNotFound();
                }
                else
                {
                    GetUserData(user);
                }
            }
        }

        private void GetUserData(User user)
        {
            throw new NotImplementedException();
        }

        private string AddNewUser()
        {
            var login = EnterLoginNewUser();
            var firstName = EnterUserName();
            var lastName = EnterLastName();
            var patronymic = EnterPatronymic();
            var birthDate = EnterBirthDate();
            var email = EnterEmail();
            var status = EnterStatus();
            var commit = CommitChanges();

            if (commit.ToUpper() == "N")
            {
                return string.Empty;
            }

            var user = new User()
            {
                BirthDate = birthDate, Email = email, FirstName = firstName, LastName = lastName, Login = login,
                Patronymic = patronymic, RegistrationDateUTC = DateTime.UtcNow, Status = (UserStatus)status
            };

            _dbAdo.AddNewUser(user);

            return login;
        }

        private string CommitChanges()
        {
            _cs.MessageOnLine("Сохранить внесенные данные [N - нет, Enter - да]: ");
            return _cs.Read();
        }

        private int EnterStatus()
        {
            _cs.MessageOnLine("Укажите Ваш статус (1 - ученик, 2 - учитель): ");
            return int.Parse(_cs.Read());
        }

        private string EnterEmail()
        {
            _cs.MessageOnLine("Емейл: ");
            return _cs.Read();
        }

        private DateTime EnterBirthDate()
        {
            _cs.MessageOnLine("Дата рождения (DD.MM.YYYY): ");
            return DateTime.Parse(_cs.Read());
        }

        private string EnterPatronymic()
        {
            _cs.MessageOnLine("Отчество: ");
            return _cs.Read();
        }

        private string EnterLastName()
        {
            _cs.MessageOnLine("Фамилия: ");
            return _cs.Read();
        }

        private string EnterUserName()
        {
            _cs.MessageOnLine("Имя: ");
            return _cs.Read();
        }

        void UserNotFound()
        {
            _cs.Message("Пользователь не найден");
        }

        void EnterLogin()
        {
            _cs.Message("Введите Ваш логин [N - новый пользователь].");
        }
        string EnterLoginNewUser()
        {
            _cs.MessageOnLine("Логин: ");
            return _cs.Read();
        }
    }
}
