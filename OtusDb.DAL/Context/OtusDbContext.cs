﻿using Microsoft.EntityFrameworkCore;
using OtusDb.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtusDb.DAL.Context
{
    public class OtusDbContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Resource> Resources { get; set; }

        public OtusDbContext(DbContextOptions options) : base(options)
        { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CourseCollection>()
                .HasOne(pt => pt.User)
                .WithMany(p => p.Courses)
                .HasForeignKey(pt => pt.UserId);
        }
    }
}