﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Microsoft.Data.SqlClient;
using OtusDb.Application;
using OtusDb.Domain.Enums;
using OtusDb.Domain.Tables;

namespace OtusDb.DAL.Repositories
{
    public class OtusDbAdoRepository : IOtusDbRepository
    {
        private readonly string _connectionString;

        public OtusDbAdoRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public void AddNewLesson(Lesson lesson)
        {
            throw new NotImplementedException();
        }

        public void AddNewUser(User user)
        {
            var sqlExpression = $"INSERT INTO Users (Email, Login, FirstName, LastName, Patronymic, BirthDate, RegistrationDateUTC, Status) " +
                                $"VALUES (@Email, @Login, @FirstName, @LastName, @Patronymic, @BirthDate, @RegistrationDateUTC, @Status); SET @Id = SCOPE_IDENTITY();";

            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            var command = new SqlCommand(sqlExpression, connection);

            command.Parameters.Add("@Email", SqlDbType.NVarChar, 200).Value = user.Email;
            command.Parameters.Add("@Login", SqlDbType.NVarChar, 100).Value = user.Login;
            command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value = user.FirstName;
            command.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value = user.LastName;
            command.Parameters.Add("@Patronymic", SqlDbType.NVarChar, 100).Value = user.Patronymic;
            command.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = user.BirthDate;
            command.Parameters.Add("@RegistrationDateUTC", SqlDbType.DateTime).Value = user.RegistrationDateUTC;
            command.Parameters.Add("@Status", SqlDbType.Int).Value = user.Status;
            command.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

            var number = command.ExecuteNonQuery();
            var id = int.Parse(command.Parameters["@ID"].Value.ToString());

            user.Id = id;
        }

        public IEnumerable<Course> GetAllCourses()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Lesson> GetAllLessons()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Resource> GetAllResources()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAllTeachers()
        {
            throw new NotImplementedException();
        }

        public Course GetCourseById(int id)
        {
            var sqlExpression = $"SELECT * FROM Courses WHERE Id = @Id;";

            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            var command = new SqlCommand(sqlExpression, connection);

            command.Parameters.Add("@Id", SqlDbType.Int).Value = id;

            var reader = command.ExecuteReader();

            if (reader.Read() is false) { return null; }
            var name = (string)reader["Name"];
            var dateStartUTC = (DateTime)reader["DateStartUTC"];
            var price = (decimal)reader["Price"];

            reader.Close();

            var course = new Course()
            {
                Id = (int)id, DateStartUTC = dateStartUTC, Name = name, Price = price
            };

            return course;
        }

        public User GetUserByLogin(string login)
        {
            var sqlExpression = $"SELECT * FROM Users WHERE Login = @Login;";

            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            var command = new SqlCommand(sqlExpression, connection);

            command.Parameters.Add("@Login", SqlDbType.NVarChar).Value = login;

            var user = new User();
            using (var reader = command.ExecuteReader())
            {
                if (reader.Read() is false) { return null; }

                user.Id = (int) reader["Id"];
                user.Email = (string) reader["Email"];
                user.FirstName = (string) reader["FirstName"];
                user.Patronymic = (string) reader["Patronymic"];
                user.BirthDate = (DateTime) reader["BirthDate"];
                user.RegistrationDateUTC = (DateTime) reader["RegistrationDateUTC"];
                user.Status = (UserStatus) reader["Status"];
            }
            return user;
        }

        public void SaveAll()
        {
            throw new NotImplementedException();
        }
    }
}
