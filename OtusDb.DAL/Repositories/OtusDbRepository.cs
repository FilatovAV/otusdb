﻿using Microsoft.EntityFrameworkCore;
using OtusDb.Application;
using OtusDb.DAL.Context;
using OtusDb.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace OtusDb.DAL.Repositories
{
    public class OtusDbRepository : IOtusDbRepository, IDisposable
    {
        private readonly OtusDbContext _db;
        public OtusDbRepository(OtusDbContext db)
        {
            _db = db;
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        public void AddNewLesson(Lesson lesson)
        {
            _db.Add(lesson);
            SaveAll();
        }

        public IEnumerable<Course> GetAllCourses()
        {
            return _db.Courses.Include(i => i.Lessons);
        }

        public Course GetCourseById(int id)
        {
            throw new NotImplementedException();
        }

        public void SaveAll()
        {
            _db.SaveChanges();
        }

        public User GetUserByLogin(string login)
        {
            return _db.Users
                .Include(w => w.Courses)
                .FirstOrDefault(w => w.Login == login);
        }
        public User GetUserByLoginWithoutCourses(string login)
        {
            return _db.Users.FirstOrDefault(w => w.Login == login);
        }

        public IEnumerable<Resource> GetAllResources()
        {
            return _db.Resources;
        }

        public IEnumerable<Lesson> GetAllLessons()
        {
            return _db.Lessons.Include(i => i.Resources);
        }

        public void AddNewUser(User user)
        {
            _db.Users.Add(user);
            SaveAll();
        }

        public IEnumerable<User> GetAllUser()
        {
            return _db.Users.Include(i=> i.Courses);
        }
        public IEnumerable<User> GetAllTeachers()
        {
            return _db.Users.Where(w => w.Status == Domain.Enums.UserStatus.IsTeacher);
        }
    }
}
